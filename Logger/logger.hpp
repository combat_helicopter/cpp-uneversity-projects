#pragma ones

#include <tuple>
#include <memory>
#include <memory>
#include <atomic>
#include <vector>
#include <chrono>
#include <utility>
#include <fstream>
#include <ostream>
#include <iostream>
#include <filesystem>
#include <string_view>

#define STR_BODY(x) #x
#define STR_PROXY(x) STR_BODY(x)
#define STR_LINE STR_PROXY(__LINE__)
#define STR_FILE STR_PROXY(__FILE__)
#define CONCAT_BODY(a, b) "FILE: " a " \nLINE: " b
#define TIMER_ID CONCAT_BODY(STR_FILE, STR_LINE)

#ifdef WITH_TIMER_
  #define TIMER() static Initializer in(std::string_view(TIMER_ID)); \
                  auto timer = Controller::Instance().GetTimer(in.GetLabel())
#else
  #define TIMER()
#endif

class Initializer;

class Controller {
private:
  const int MAX_NUMBER_OF_FUNCTIONS = FUNCTIONS;
  Controller() = default;

public:
  struct MetaInformation {
    MetaInformation() = default;
    MetaInformation(std::string_view& metadata_string)
    : metadata_string_(metadata_string) {}

    std::string_view metadata_string_;
  };

  class Timer {
  public:
    Timer(std::function<void(uint64_t)>&& aggregator)
    : aggregator_(aggregator) {
      start_ = std::chrono::steady_clock::now();
    }

    ~Timer() {
      auto end = std::chrono::steady_clock::now();
      auto delta = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start_).count();
      aggregator_(delta);
    }

  private:
    std::function<void(uint64_t)> aggregator_;
    std::chrono::time_point<std::chrono::steady_clock> start_;
  };

public:
  static Controller& Instance() {
    static Controller instance;
    return instance;
  }

  ~Controller() {
    std::ofstream out;
    std::filesystem::path output_path(OUTPUT_PATH);

    if (std::filesystem::exists(output_path)) {
      out.open(output_path);
      for (int i = 0; i < label_; ++i) {
        out << "----------------------\n";
        out << "FUNCTION    : "  << meta_information_[i].metadata_string_ << "\n";
        out << "WORKING TIME: " << static_cast<double>(timings_[i]) / 1e9 << " s\n";
        out << "CALLS       : " << calls_[i] << "\n";
        out << std::string(OUTPUT_PATH) << "\n";
      }
      out.close();
      return;
    }

    for (int i = 0; i < label_; ++i) {
      std::cout << "----------------------\n";
      std::cout << "FUNCTION    : "  << meta_information_[i].metadata_string_ << "\n";
      std::cout << "WORKING TIME: " << static_cast<double>(timings_[i]) / 1e9 << " s\n";
      std::cout << "CALLS       : " << calls_[i] << "\n";
      std::cout << std::string(OUTPUT_PATH) << "\n";
    }
  }

  int GetLineNumber() {
    int line = label_.fetch_add(1);
    return line;
  }

  void AddMetaInformation(const MetaInformation& meta_information, int label) {
    meta_information_[label] = meta_information;
  }

  std::unique_ptr<Timer> GetTimer(int label) {
    return std::make_unique<Timer>(
        [this, label](uint64_t delta) {
          this->timings_[label].fetch_add(delta);
          this->calls_[label].fetch_add(1);
        });
  }

private:
  std::atomic<int> label_{0};
  std::vector<std::atomic<uint64_t>> timings_ = std::vector<std::atomic<uint64_t>>(MAX_NUMBER_OF_FUNCTIONS);
  std::vector<std::atomic<uint64_t>> calls_ = std::vector<std::atomic<uint64_t>>(MAX_NUMBER_OF_FUNCTIONS);
  std::vector<MetaInformation> meta_information_ = std::vector<MetaInformation>(MAX_NUMBER_OF_FUNCTIONS);
};

class Initializer {
public:
  Initializer(std::string_view&& metadata_string)
  : function_label_(Controller::Instance().GetLineNumber()) {
    Controller::MetaInformation meta_information(metadata_string);
    Controller::Instance().AddMetaInformation(meta_information, function_label_);
  }
  Initializer(const Initializer& initializer) = default;

public:
  int GetLabel() const {
    return function_label_;
  }

private:
  int function_label_;
};