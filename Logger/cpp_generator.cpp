#include <string>
#include <fstream>
//#include "logger.hpp"

auto get_includes = [](){
  return "#include \"../logger.hpp\"\n"
         "#include <vector>\n\n";
};

auto get_random_cycle_pattern = [](int iteration) -> std::string {
  std::string ans = "  int counter = 0;\n"
                    "  for (int i = 0; i < " + std::to_string(iteration) + "; ++i) {\n"
                    "     counter += (random() % 1000);\n"
                    "  }\n"
                    "  return counter;\n";
  return ans;
};

auto get_function = [](int function_label, int iteration){
  std::string function_code =
      "int function_" + std::to_string(function_label) + "() {\n" +
      "   TIMER();\n" +
      get_random_cycle_pattern(iteration) +
      "}\n\n";
  return function_code;
};

std::string get_pack(int number, int last_label) {
  std::string code = "int pack_" + std::to_string(number) + "() {\n" +
      "  auto start = std::chrono::steady_clock::now();\n";
  for (int i = 0; i < last_label; ++i) {
    code += "  function_" + std::to_string(i) + "();\n";
  }
  code += "  auto end = std::chrono::steady_clock::now();\n"
          "  auto delta = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();\n"
          "  return delta;\n"
          "}\n\n";
  return code;
}

std::string get_test(int number_of_packs) {
  std::string code = "std::vector<int> generated_test() {\n"
                     "  std::vector<int> durations;\n";
  for (int i = 0; i < number_of_packs; ++i) {
    code += "  durations.push_back(pack_" + std::to_string(i) + "());\n";
  }
  code += "return durations;\n"
          "}\n\n";
  return code;
}

std::string generator(int pack_number, int pack_size, int iteration) {
  int function_number = pack_number * pack_size;
  std::string code;
  code += get_includes();
  for (int i = 0; i < function_number; ++i) {
    code += get_function(i, iteration);
  }

  for (int i = 0; i < pack_number; ++i) {
    code += get_pack(i, (i + 1) * pack_size);
  }

  code += get_test(pack_number);
  return code;
}

void create_new_test(std::string& file_path, int pack_number, int pack_size, int iteration) {
  std::ofstream file;
  file.open (file_path);
  file << generator(pack_number, pack_size, iteration);
  file.close();
}