Запуск c таймером(по дефолту без таймера) производится с флагом:
```
-DWITH_TIMER=1
```

Для записи в файл(по дефолту в поток вывода) используется флаг:
```
-DWITH_OUTPUT_PATH:string="/Users/eduard/Desktop/gits/cpp-uneversity-projects/Logger/out"
```
Для запуска с изменением максимального числа функций(по дефолту миллион) используется флаг:
```
-DFUNCTIONS_COUNT=10000
```

## Часть 1, Logger:

При первом вызове Time() происходит выделение памяти в которую
будут записаны информация о функциях, о количестве вызовов каждой
функции и о времени исполнения каждой функции. При каждом
последующем вызове замеряется время исполнения функции и
прибавляется к общему времени. По завершении всей программы, в файл,
указанный при сборке(или в поток вывода в случае отсутствия файла),
пишется информация о функции(путь к файлу с функцией, номер строки,
число её вызовов во всех потоках, суммарное время работы во всех
потоках). Пример:
```
FILE: "/Users/eduard/Desktop/gits/cpp-uneversity-projects/Logger/./tests/test.cpp"
LINE: 5
WORKING TIME: 9.628e-06 s
CALLS       : 1
/Users/eduard/Desktop/gits/cpp-uneversity-projects/Logger/out
```


## Часть 2, Generator:
Чтобы сгенерировать тест нужно вызвать функцию:
```
void create_new_test(std::string& file_path, int pack_number, int pack_size, int iteration)
```
где:
* file_path   - путь к файлу, в который запишется тест,
* pack_number - число "больших" функций, время которых будем замерять,
* pack_size   - число "маленьких" функций внутри "больших",
* iteration   - число итераций внутри "маленькой" функции.

Пример "маленькой" функции:
```
int function_9() {
    TIMER();
    int counter = 0;
    for (int i = 0; i < 100; ++i) {
        counter += (random() % 1000);
    }
    return counter;
}
```
Пример "большой" функции:
```
int pack_0() {
    auto start = std::chrono::steady_clock::now();
    function_0();
    function_1();
    function_2();
    auto end = std::chrono::steady_clock::now();
    auto delta = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    return delta;
}
```
То есть create_new_test запустит и замерит время работы pack_number "больших".

## Опыт:
На графике graph.png зависимость времени работы t от числа "маленьких" функций внутри "большой функции". 

