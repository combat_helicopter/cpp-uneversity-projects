﻿#include<string>
#include<thread>
#include<fstream>
#include<iostream>
#include<functional>
#include"./tests/test.cpp"
#include"cpp_generator.cpp"

//#include"logger.hpp"
#include<vector>
#include<atomic>

//добавить конфигуратор (командную строку), вывод в файл

int main() {
  int pack_number = 50;
  int pack_size = 5;
  int iteration = 100;


//  const int MAX_NUMBER_OF_FUNCTIONS = 1e6;
//  std::vector<std::atomic<uint64_t>> timings_ = std::vector<std::atomic<uint64_t>>(MAX_NUMBER_OF_FUNCTIONS);
//  std::vector<Controller::MetaInformation> meta_information_ =
//      std::vector<Controller::MetaInformation>(MAX_NUMBER_OF_FUNCTIONS);
//  std::cout << timings_.size() << "\n";
//  std::cout << meta_information_.size() << "\n";

//  std::string file_path = "/Users/eduard/Desktop/gits/cpp-uneversity-projects/Logger/tests/test.cpp";
//  create_new_test(file_path, pack_number, pack_size, iteration);
//
  auto t = generated_test();
  std::string with_timer_path = "/Users/eduard/Desktop/gits/cpp-uneversity-projects/Logger/results/"
                                "res_with_timer.txt";
  std::string without_timer_path = "/Users/eduard/Desktop/gits/cpp-uneversity-projects/Logger/results/"
                                   "res_without_timer.txt";
  std::ofstream file;
  file.open(without_timer_path);
  for (int i = 0; i < t.size(); ++i) {
    file << std::to_string((i + 1) * (pack_size)) << " " << std::to_string(t[i]) << "\n";
  }
  file.close();
}
