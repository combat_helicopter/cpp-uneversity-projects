#include "Quaternion.h"
#include "../Logger/Logger.h"
#include <cmath>
#include <iostream>

///////////////////////////////////////////////
///////////////////////////////////////////////
//////////////------VECTOR-----////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
float Vector::x() const {
  TIMER();
  return x_;
}

float Vector::y() const {
  TIMER();
  return y_;
}

float Vector::z() const {
  TIMER();
  return z_;
}

Vector& Vector::load(float x, float y, float z) {
  TIMER();
  x_ = x;
  y_ = y;
  z_ = z;
  return *this;
}

void Vector::operator+=(const Vector &v) {
  TIMER();
  x_ += v.x_;
  y_ += v.y_;
  z_ += v.z_;
}

std::ostream& operator << (std::ostream& os, const Vector& v) {
  TIMER();
  os << "Vector: " << v.x() << " " << v.y() << " " << v.z();
  return os;
}

Vector operator + (const Vector& v1, const Vector& v2) {
  TIMER();
  Vector v;
  v += v1;
  v += v2;
  return v;
}


///////////////////////////////////////////////
///////////////////////////////////////////////
///////////------QUATERNION-----///////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
float Quaternion::cos() const {
  TIMER();
  return cos_;
}

float Quaternion::sin() const {
  TIMER();
  return sin_;
}

const Vector & Quaternion::e() const {
  TIMER();
  return e_;
}

Quaternion& Quaternion::load(float cos, float sin, const Vector &e) {
  TIMER();
  cos_ = cos;
  sin_ = sin;
  e_ = e;

  return *this;
}

Quaternion& Quaternion::invert() {
  TIMER();
  sin_ = -1 * sin_;
  return *this;
}

std::ostream& operator<<(std::ostream& os, const Quaternion& q) {
  TIMER();
  os << "Quaternion: " << q.cos() << " " << q.sin() << " " << q.e();
  return os;
}

///////////////////////////////////////////////
///////////////////////////////////////////////
////////////--------OPTIONS------//////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
Vector dot(float a, const Vector& v) {
  TIMER();
  return Vector(a * v.x(), a * v.y(), a * v.z());
}

float scalProd(const Vector& v1, const Vector& v2) {
  TIMER();
  return v1.x() * v2.x() + v1.y() * v2.y() + v1.z() * v2.z();
}

Vector vecProd(const Vector& v1, const Vector& v2) {
  TIMER();
  float x = v1.y() * v2.z() - v1.z() * v2.y();
  float y = v1.z() * v2.x() - v1.x() * v2.z();
  float z = v1.x() * v2.y() - v1.y() * v2.x();
  return Vector(x, y, z);
}

Quaternion qProd(const Quaternion& q1, const Quaternion& q2) {
  TIMER();
  float cos = q1.cos() * q2.cos() - q1.sin() * q2.sin() * scalProd(q1.e(), q2.e());
  Vector e = dot(q1.cos(), dot(q2.sin(), q2.e())) +
             dot(q2.cos(), dot(q1.sin(), q1.e())) +
             dot(q1.sin() * q2.sin(), vecProd(q1.e(), q2.e()));
  float sin = sqrt(scalProd(e, e));
  e = dot(1/sin, e);
  return Quaternion(cos, sin, e);
}

Vector& rotate(Vector& v, float angle, const Vector& axis) {
  TIMER();
  if (fabs(v.x()) < 1 && fabs(v.y()) < 1 && fabs(v.z()) < 1) {
    return v;
  }

  Quaternion q(cos(angle / 2), sin(angle / 2), axis);
  Quaternion q_(cos(angle / 2), -1 * sin(angle / 2), axis);

  float v_size = sqrt(scalProd(v, v));
  Quaternion quaternion_v(0, 1, dot(1/v_size, v));
  v = dot(v_size, qProd(qProd(q, quaternion_v), q_).e());
  return v;
}

Vector& rotateX(Vector& v, float angle) {
  TIMER();
  v = Vector(v.x(),
             cos(angle) * v.y() + sin(angle) * v.z(),
             -sin(angle) * v.y() + cos(angle) * v.z());
  return v;
}

Vector& rotateY(Vector& v, float angle) {
  TIMER();
  v = Vector(cos(angle) * v.x() + sin(angle) * v.z(),
             v.y(),
             -sin(angle) * v.x() + cos(angle) * v.z());
  return v;
}

Vector& rotateZ(Vector& v, float angle) {
  TIMER();
  v = Vector(cos(angle) * v.x() + sin(angle) * v.y(),
             -sin(angle) * v.x() + cos(angle) * v.y(),
             v.z());
  return v;
}