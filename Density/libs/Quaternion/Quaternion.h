#include <ostream>

/*!
\file
\brief Заголовочный файл с описанием классов Vector,
 Quaternion и функций для работы с ними

 Данный файл содержит в себе определения основных
 классов Vector и Quaternion. Объекты класса Vector нужны
 для эмуляции геометрического трёхмерного вектора,
 объект класса Quaternion нужен для эмуляции
 поворотов объектов класса Vector
*/
#ifndef DENSITY_LIBS_QUATERNION_H_
#define DENSITY_LIBS_QUATERNION_H_


class Vector {
 public:
  //! x_ == y_ == z_ == 0
  explicit Vector() = default;
  //! x_ == x y_ == y z == z_
  explicit Vector(float x, float y, float z)
  : x_(x), y_(y), z_(z) {}

 public:
  //! сложение векторов происходит по обычным правилам сложения геометрических векторов
  void operator += (const Vector& v);
  //! функция для вывода значений вектора в поток
  friend std::ostream& operator<<(std::ostream& os, const Vector& v);

 public:
  //! возвращает копию x_
  float x() const;
  //! возвращает копию y_
  float y() const;
  //! возвращает копию z_
  float z() const;
  //! x_ = x y_ = y z_ = z
  Vector& load(float x, float y, float z);

 private:
  float x_{0};
  float y_{0};
  float z_{0};
};


class Quaternion {
 public:
//  explicit Quaternion() = default;
  /*!
      cos - косинус угла поворота поделенного на два
      sin - синус угла поворота поделенного на два
      e   - ось вокруг которой происходит вращение проти часовой стрелки
  */
  explicit Quaternion(float cos, float sin, const Vector& e)
  : cos_(cos), sin_(sin), e_(e) {}
  /*!
      cos_ == 0
      sin_ == 1
      v - ось вокруг которой происходит вращение проти часовой стрелки
  */
  explicit Quaternion(const Vector& v)
  : cos_(0), sin_(1), e_(v) {}

 public:
  //! функция для вывода значений вектора в поток
  friend std::ostream& operator<<(std::ostream& os, const Quaternion& q);

 public:
  //! возвращает копию cos_
  float cos() const;
  //! возвращает копию sin_
  float sin() const;
  //! возвращает константную ссылку на ось вращения
  const Vector& e() const;
  //! cos_ = cos sin_ = sin e_ = e
  Quaternion& load(float cos, float sin, const Vector& e);
  //! сопрячь кватернион
  Quaternion& invert();

 private:
  float cos_{0};
  float sin_{0};
  Vector e_{0, 0, 0};
};


/*!
 \param[out] Vector
 \param[in] a - скаляр
 \param[in] v - вектор

 умножение скаляра a на вектор v
*/
Vector dot(float a, const Vector& v);
/*!
 \param[out] Vector
 \param[in] v1 - вектор
 \param[in] v2 - вектор

 скалярное произведение векторов v1 и v2
*/
float scalProd(const Vector& v1, const Vector& v2);
/*!
 \param[out] Quaternion
 \param[in] q1 - кватернион
 \param[in] q2 - кватернион

 произведение кватернионов
*/
Quaternion qProd(const Quaternion& q1, const Quaternion& q2);

/*!
 \param[out] Vector
 \param[in] v1 - вектор
 \param[in] v2 - вектор

 оператор сложения векторов
*/
Vector operator + (const Vector& v1, const Vector& v2);
/*!
 \param[out] Vector
 \param[in] v - поворачиваемый вектор
 \param[in] angle - угол поворота
 \param[in] axis - ось вокруг которой происходит поворот

 поворот вектора v на угол angle против часовой стрелки относительно оси axis
*/
Vector& rotate(Vector& v, float angle, const Vector& axis);
/*!
 \param[out] Vector
 \param[in] v - поворачиваемый вектор
 \param[in] angle - угол поворота

 поворот вектора v на угол angle против часовой стрелки относительно оси OX
*/
Vector& rotateX(Vector& v, float angle);
/*!
 \param[out] Vector
 \param[in] v - поворачиваемый вектор
 \param[in] angle - угол поворота

 поворот вектора v на угол angle против часовой стрелки относительно оси OY
*/
Vector& rotateY(Vector& v, float angle);
/*!
 \param[out] Vector
 \param[in] v - поворачиваемый вектор
 \param[in] angle - угол поворота

 поворот вектора v на угол angle против часовой стрелки относительно оси OZ
*/
Vector& rotateZ(Vector& v, float angle);
#endif //DENSITY_LIBS_QUATERNION_H_
