#include <cmath>
#include <vector>
#include "../Quaternion/Quaternion.h"

/*!
\file
\brief Заголовочный файл с описанием классов DotsHandler,
  Display и функций для работы с ними

 класс DotsHandler предназначен для работы с массивом векторов,
 класс Display предназначен для отображения плотности на экране
*/

#ifndef DENSITY_LIBS_DOTSHANDLER_DOTSHANDLER_H_
#define DENSITY_LIBS_DOTSHANDLER_DOTSHANDLER_H_

const float BRIGHTNESS = 255;
const int FIELD_SIZE = int(BRIGHTNESS * sqrt(3) + 1);

class DotsHandler {
 public:
  DotsHandler(const std::vector<int>& xs,
              const std::vector<int>& ys,
              const std::vector<int>& zs)
  : xs_(xs), ys_(ys), zs_(zs) {}

 public:
  //! по набору координат xs ys zs строит набор векто Vector'оф
  void buildVectors();
  //! геттер для получения массива Vector'оф
  const std::vector<Vector>& getVectors();
  //! поворот всех векторов вокруг оси axis на угол angle против часовой стрелки
  void rotate(float angle, const Vector& axis);

 private:
  std::vector<int> xs_;
  std::vector<int> ys_;
  std::vector<int> zs_;
  std::vector<Vector> vectors_;
};

class Display {
 public:
  //! по набору Vector'оф сделать матрицу, отображаемую на экран
  void create_display(const std::vector<Vector>& vs);
  //! Получить матрицу отображаемую на экран
  const std::vector<std::vector<int>>& getDisplay();
 private:
  std::vector<std::vector<int>> displayMatrix_ =
      std::vector<std::vector<int>>(FIELD_SIZE, std::vector<int>(FIELD_SIZE, 0));
//  std::vector<int> displayMatrix_ = std::vector<int> (FIELD_SIZE * FIELD_SIZE);
};

#endif //DENSITY_LIBS_DOTSHANDLER_DOTSHANDLER_H_
