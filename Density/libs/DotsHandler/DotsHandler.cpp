#include "DotsHandler.h"
#include "../Logger/Logger.h"
#include <iostream>

void DotsHandler::buildVectors() {
  TIMER();
  if (!vectors_.empty()) {
    return;
  }

  vectors_.resize(xs_.size());
  for (int i = 0; i < vectors_.size(); ++i) {
    vectors_[i] = Vector(float(xs_[i]),
                         float(ys_[i]),
                         float(zs_[i]));
  }
}

const std::vector<Vector>& DotsHandler::getVectors() {
  TIMER();
  return vectors_;
}

void DotsHandler::rotate(float angle, const Vector& axis) {
  TIMER();
  if (fabs(axis.x() - 1) + fabs(axis.y()) + fabs(axis.z()) < 1e-6) {
    for (auto &v: vectors_) {
      ::rotateX(v, angle);
    }
  } else if (fabs(axis.y() - 1) + fabs(axis.x()) + fabs(axis.z()) < 1e-6) {
    for (auto &v: vectors_) {
      ::rotateY(v, angle);
    }
  } else if (fabs(axis.z() - 1) + fabs(axis.x()) + fabs(axis.y()) < 1e-6) {
    for (auto &v: vectors_) {
      ::rotateZ(v, angle);
    }
  }
}

//void Display::create_display(const std::vector<Vector>& vs) {
//  TIMER();
//  for (auto& cell: displayMatrix_) {
//    cell = 0;
//  }
//
//  for (auto& v: vs) {
//    displayMatrix_[int(-v.z() + FIELD_SIZE / 2) * FIELD_SIZE + int(v.y() + FIELD_SIZE / 2)] += 1;
//  }
//
//  for (auto& cell: displayMatrix_) {
//      cell = int(255 * 2 * (1 / (1 + pow(2.71, -0.5 * cell)) - 0.5));
//  }
//
//}
//
//const std::vector<int>& Display::getDisplay() {
//  TIMER();
//  return displayMatrix_;
//}

void Display::create_display(const std::vector<Vector>& vs) {
  TIMER();
  for (auto& vec: displayMatrix_) {
    for (auto &cell: vec) {
      cell = 0;
    }
  }

  for (int i = 0; i < vs.size() - 255 * 3; ++i) {
    const Vector& v = vs[i];
    displayMatrix_[int(-v.z() + FIELD_SIZE / 2)][int(v.y() + FIELD_SIZE / 2)] += 1;
  }

  for (int i = vs.size() - 255 * 3 + 1; i < vs.size(); ++i) {
    auto& v = vs[i];
    int alpha = (vs.size() - i) / 255 + 1;
    displayMatrix_[int(-v.z() + FIELD_SIZE / 2)][int(v.y() + FIELD_SIZE / 2)] = alpha * vs.size();
  }

  for (auto& line: displayMatrix_) {
    for (auto& cell: line) {
      if (cell >= vs.size()) {
        continue;
      }

      cell = int(255 * 2 * (1 / (1 + pow(2.71, -1e4 * (float(cell) / float(vs.size())))) - 0.5));
    }
  }

}

const std::vector<std::vector<int>>& Display::getDisplay() {
  TIMER();
  return displayMatrix_;
}