# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.20

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/Cellar/cmake/3.20.1/bin/cmake

# The command to remove a file.
RM = /usr/local/Cellar/cmake/3.20.1/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/eduard/Desktop/gits/cpp-university-projects/Density

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/eduard/Desktop/gits/cpp-university-projects/Density/build

# Include any dependencies generated for this target.
include libs/CMakeFiles/density.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include libs/CMakeFiles/density.dir/compiler_depend.make

# Include the progress variables for this target.
include libs/CMakeFiles/density.dir/progress.make

# Include the compile flags for this target's objects.
include libs/CMakeFiles/density.dir/flags.make

libs/CMakeFiles/density.dir/target.cpp.o: libs/CMakeFiles/density.dir/flags.make
libs/CMakeFiles/density.dir/target.cpp.o: ../src/target.cpp
libs/CMakeFiles/density.dir/target.cpp.o: libs/CMakeFiles/density.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/Users/eduard/Desktop/gits/cpp-university-projects/Density/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object libs/CMakeFiles/density.dir/target.cpp.o"
	cd /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs && /Library/Developer/CommandLineTools/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT libs/CMakeFiles/density.dir/target.cpp.o -MF CMakeFiles/density.dir/target.cpp.o.d -o CMakeFiles/density.dir/target.cpp.o -c /Users/eduard/Desktop/gits/cpp-university-projects/Density/src/target.cpp

libs/CMakeFiles/density.dir/target.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/density.dir/target.cpp.i"
	cd /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs && /Library/Developer/CommandLineTools/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /Users/eduard/Desktop/gits/cpp-university-projects/Density/src/target.cpp > CMakeFiles/density.dir/target.cpp.i

libs/CMakeFiles/density.dir/target.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/density.dir/target.cpp.s"
	cd /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs && /Library/Developer/CommandLineTools/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /Users/eduard/Desktop/gits/cpp-university-projects/Density/src/target.cpp -o CMakeFiles/density.dir/target.cpp.s

# Object files for target density
density_OBJECTS = \
"CMakeFiles/density.dir/target.cpp.o"

# External object files for target density
density_EXTERNAL_OBJECTS =

libs/density: libs/CMakeFiles/density.dir/target.cpp.o
libs/density: libs/CMakeFiles/density.dir/build.make
libs/density: /usr/local/lib/libopencv_gapi.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_stitching.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_alphamat.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_aruco.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_barcode.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_bgsegm.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_bioinspired.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_ccalib.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_dnn_objdetect.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_dnn_superres.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_dpm.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_face.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_freetype.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_fuzzy.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_hfs.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_img_hash.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_intensity_transform.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_line_descriptor.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_mcc.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_quality.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_rapid.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_reg.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_rgbd.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_saliency.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_sfm.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_stereo.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_structured_light.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_superres.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_surface_matching.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_tracking.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_videostab.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_viz.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_wechat_qrcode.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_xfeatures2d.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_xobjdetect.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_xphoto.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_shape.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_highgui.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_datasets.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_plot.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_text.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_ml.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_phase_unwrapping.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_optflow.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_ximgproc.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_video.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_videoio.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_dnn.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_imgcodecs.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_objdetect.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_calib3d.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_features2d.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_flann.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_photo.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_imgproc.4.5.3.dylib
libs/density: /usr/local/lib/libopencv_core.4.5.3.dylib
libs/density: libs/CMakeFiles/density.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/Users/eduard/Desktop/gits/cpp-university-projects/Density/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable density"
	cd /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/density.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
libs/CMakeFiles/density.dir/build: libs/density
.PHONY : libs/CMakeFiles/density.dir/build

libs/CMakeFiles/density.dir/clean:
	cd /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs && $(CMAKE_COMMAND) -P CMakeFiles/density.dir/cmake_clean.cmake
.PHONY : libs/CMakeFiles/density.dir/clean

libs/CMakeFiles/density.dir/depend:
	cd /Users/eduard/Desktop/gits/cpp-university-projects/Density/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/eduard/Desktop/gits/cpp-university-projects/Density /Users/eduard/Desktop/gits/cpp-university-projects/Density/src /Users/eduard/Desktop/gits/cpp-university-projects/Density/build /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs /Users/eduard/Desktop/gits/cpp-university-projects/Density/build/libs/CMakeFiles/density.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : libs/CMakeFiles/density.dir/depend

