cmake_minimum_required(VERSION 3.19)
project(Density)

set(CMAKE_CXX_STANDARD 17)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_library(LIBS
        libs/Quaternion/Quaternion.h
        libs/Quaternion/Quaternion.cpp
        libs/DotsHandler/DotsHandler.h
        libs/DotsHandler/DotsHandler.cpp
        libs/Logger/Logger.h)

add_subdirectory(
    src
    libs
)

