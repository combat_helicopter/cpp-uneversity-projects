#include <iostream>
#include <opencv2/opencv.hpp>

#define CVUI_IMPLEMENTATION
#include "../libs/CVUI/cvui.h"
#include "../libs/Quaternion/Quaternion.h"
#include "../libs/DotsHandler/DotsHandler.h"
#include "../libs/Logger/Logger.h"

#define WINDOW_NAME "CVUI Canny Edge"

const float PI = 3.141592;
const int discr = 3600;
std::string path = "";//Users/eduard/Desktop/Снимок экрана 2021-12-14 в 00.03.11.png";

/*!
    Каждый пиксель фотографии path предлттавляется ввиде Vector и помещается в единичный куб
    Далее вдоль оси OZ считается плотность и отображается на графике,
    График можно крутить и получать распределение плотности вдоль других осей
*/

int main() {
  std::cout << "Bad link:\n";
  std::cout << "Enter the correct path to the PNG file:\n";
  getline(std::cin, path);
  cv::Mat lena = cv::imread(path);
  cv::Mat frame = lena.clone();

  while (frame.empty()) {
    std::cout << "Bad link.\n";
    std::cout << "Enter the correct path to the PNG file:\n";
    path = "";
    getline(std::cin, path);
    lena = cv::imread(path);
    frame = lena.clone();
  }

  std::vector<int> xs(frame.rows * frame.cols, 0);
  std::vector<int> ys(frame.rows * frame.cols, 0);
  std::vector<int> zs(frame.rows * frame.cols, 0);

  for (int i = 0; i < frame.rows; ++i) {
    for (int j = 0; j < frame.cols; ++j) {
      xs[i * frame.cols + j] = int(frame.at<cv::Vec3b>(i, j)[0]);
      ys[i * frame.cols + j] = int(frame.at<cv::Vec3b>(i, j)[1]);
      zs[i * frame.cols + j] = int(frame.at<cv::Vec3b>(i, j)[2]);
    }
  }

  for (int i = 1; i < 255; ++i) {
    xs.push_back(i);
    ys.push_back(0);
    zs.push_back(0);
  }

  for (int i = 1; i < 255; ++i) {
    xs.push_back(0);
    ys.push_back(i);
    zs.push_back(0);
  }

  for (int i = 1; i < 255; ++i) {
    xs.push_back(0);
    ys.push_back(0);
    zs.push_back(i);
  }

  for (int i = 0; i < xs.size(); ++i) {
    xs[i] -= 127;
    ys[i] -= 127;
    zs[i] -= 127;
  }

  DotsHandler handler(xs, ys, zs);
  Display display;
  handler.buildVectors();
  display.create_display(handler.getVectors());
  int display_size = display.getDisplay().size();
  cv::Mat density(display_size, display_size, CV_8UC(3));

  cv::namedWindow(WINDOW_NAME);
  cvui::init(WINDOW_NAME);

  bool use_colorbar = false;
  bool settings_on = true;
  int x_angle = 0;
  int x_old_angle = 0;
  int y_angle = 0;
  int y_old_angle = 0;
  int z_angle = 0;
  int z_old_angle = 0;

  int delta = 2;

  while (true) {
    z_angle += delta;
    z_angle %= discr;

    if (x_angle != x_old_angle) {
      handler.rotate(2*PI*float((x_angle - x_old_angle)) / discr, Vector(1, 0, 0));
      x_old_angle = x_angle;
    }
    if (y_angle != y_old_angle) {
      handler.rotate(2*PI*float((y_angle - y_old_angle)) / discr, Vector(0, 1, 0));
      y_old_angle = y_angle;
    }
    if (z_angle != z_old_angle) {
      handler.rotate(2*PI*float((z_angle - z_old_angle)) / discr, Vector(0, 0, 1));
      z_old_angle = z_angle;
    }

    display.create_display(handler.getVectors());
    for (int i = 0; i < density.rows; ++i) {
      for (int j = 0; j < density.cols; ++j) {
        if (display.getDisplay()[i][j] >= xs.size()) {
          int color = display.getDisplay()[i][j];
          density.at<cv::Vec3b>(i, j)[0] = (!(color / xs.size() - 1) ? 127 : 50);
          density.at<cv::Vec3b>(i, j)[1] = (!(color / xs.size() - 2) ? 127 : 50);
          density.at<cv::Vec3b>(i, j)[2] = (!(color / xs.size() - 3) ? 127 : 50);
          continue;
        }

        int color = display.getDisplay()[i][j];
        int color3 = color;
        int color2 = color;
        int color1 = color;

        if (use_colorbar) {
          color3 = std::min(std::max(0, (3 * color - 2 * 255)), 255);
          color2 = std::min(std::max(0, (3 * color -     255)), 255);
          color1 = std::min(std::max(0, (3 * color          )), 255);
        }

        density.at<cv::Vec3b>(i, j)[0] = color1;
        density.at<cv::Vec3b>(i, j)[1] = color2;
        density.at<cv::Vec3b>(i, j)[2] = color3;
      }
    }

    if (settings_on) {
//        cvui::window(density, 5, 15, 100, 220, "Settings");
        cvui::trackbar(density, 10, 50,  90, &x_angle, 0, discr);
        cvui::trackbar(density, 10, 110, 90, &y_angle, 0, discr);
        cvui::trackbar(density, 10, 170, 90, &z_angle, 0, discr);
    }
    cvui::checkbox(density, 5, 30, "Use Color", &use_colorbar);
    cvui::checkbox(density, 5, 10, "Settings ON", &settings_on);
    cvui::update();

    cv::imshow(WINDOW_NAME, density);

    if (cv::waitKey(30) == 27) {
      break;
    }
  }
  return 0;
}
