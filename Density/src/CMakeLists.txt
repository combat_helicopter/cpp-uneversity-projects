add_executable(density target.cpp)

target_link_libraries(density PRIVATE LIBS)
target_link_libraries(density PRIVATE ${OpenCV_LIBS})
